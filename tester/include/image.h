#pragma once

#include <inttypes.h>
#include <malloc.h>//<stdlib.h>
#include <stddef.h>

#include "dimensions.h"

struct pixel {
  uint8_t components[3];
};

struct bmp_image_header_format {
  struct dimensions size;
  struct pixel* data;
};

struct bmp_image_header_format image_create(struct dimensions size );
void image_destroy( struct bmp_image_header_format* image );
