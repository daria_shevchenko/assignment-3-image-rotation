#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static void write_header(FILE *outFile, struct image const *img) {
    struct bmp_header header;
    header.bfType = BFTYPE;
    header.bfileSize = sizeof(struct bmp_header) + (img->width * img->height * sizeof(struct pixel));
    header.bfReserved = BFRESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BIPLANES;
    header.biBitCount = BITCOUNT;
    header.biCompression = BICOMPRESSION;
    header.biSizeImage = BISIZEIMAGE;
    header.biXPelsPerMeter = BIXPELSPERMETER;
    header.biYPelsPerMeter = BIYPELSPERMETER;
    header.biClrUsed = BICLRUSED;
    header.biClrImportant = BICLRIMPORTANT;

    fwrite(&header, sizeof(struct bmp_header), FILENUMBER, outFile);
}

static int read_header(FILE *inpFile, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), FILENUMBER, inpFile) != FILENUMBER || header->bfType != BFTYPE ||
        header->biCompression != BICOMPRESSION) {
        return 0;
    }

    return 1;

}

struct image make_img(uint64_t width, uint64_t height){
    struct image img;
    img.width = width;
    img.height = height;
    return img;
}
enum read_status from_bmp(FILE *inpFile, struct image *img) {
    struct bmp_header header= {0};

    if (!read_header(inpFile, &header)) {
        perror("Incorrect BMP file signature\n");
        return READ_INVALID_SIGNATURE;
    }

    *img = make_img(header.biWidth, header.biHeight);
    size_t bytes = img->width * IMGWIDTH;
    size_t padding = (PADDINGNUMBER - (bytes % PADDINGNUMBER)) % PADDINGNUMBER;
    img->data = (struct pixel *) malloc(img->width * img->height * sizeof(struct pixel));
    uint64_t i = 0;
    while (!feof(inpFile) && i < img->height) {
        fread(&img->data[i * img->width], sizeof(struct pixel), img->width, inpFile);
        int pad = fseek(inpFile, (long) padding, SEEK_CUR);
        if (pad == -1) {
            return READ_NUMBER;
        }
        i++;
    }

    return READ_OK;

}

void write_pixels(struct image const *img, uint64_t x, FILE *outFile) {
    for (uint64_t y = 0; y < img->width; y++) {
        struct pixel p = img->data[x * img->width + y];
        size_t write_head = fwrite(&p, sizeof(struct pixel), 1, outFile);
        if (write_head != FILENUMBER) {
            perror("Problem");
        }
    }
}

enum write_status to_bmp(FILE *outFile, struct image const *img) {
    size_t image = (img->width * sizeof(struct pixel));
    size_t padding = (PADDINGNUMBER - image % PADDINGNUMBER) % PADDINGNUMBER;
    write_header(outFile, img);

    for (uint64_t x = 0; x < img->height; x++) {
        write_pixels(img, x, outFile);
        if (padding > 0) {
            uint8_t padding_bytes = 0;
            for (size_t i = 0; i < padding; i++) {
                if (!fwrite(&padding_bytes, FILENUMBER, FILENUMBER, outFile)) {
                    return WRITE_ERROR;
                }
            }

        }
    }
    return WRITE_OK;
}



