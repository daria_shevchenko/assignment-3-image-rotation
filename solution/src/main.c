#include "bmp.h"
#include "err.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != CORRECTARGS) {
        perror("Correct: <source-image> <transformed-image> <angle>\n");
        return BAD_ARGS;
    }

    char *inp_img = argv[INPUT_FILE_NAME];
    char *out_img = argv[OUTPUT_FILE_NAME];
    int angle = atoi(argv[ANGLE]);

    if (angle % HALFDEGREE != 0) {
        perror("The angle of rotation should be a multiple of 90 degrees\n");
        return BAD_ANGLE;
    } else if (angle >= DEGREE) {
        angle -= DEGREE;
    } else {
        angle += DEGREE;
    }

    struct image img;
    FILE *inp_file = fopen(inp_img, "rb");

    if (!inp_file) {
        perror("Can not open file\n");
        return FILE_ERR;
    }

    enum read_status read_result = from_bmp(inp_file, &img);
    fclose(inp_file);

    if (read_result != READ_OK) {
        perror("Can't read image\n");
        return IMAGE_READ_ERR;
    }

    if (angle != 0) {
        int32_t num_rotations = (abs(angle) % DEGREE) / HALFDEGREE;
        if (angle < 0) {
            num_rotations = NEGANGLE - num_rotations;
        }
        for (int i = 0; i < num_rotations; i++) {
            struct image rotated_image = rotate(img);

            free(img.data);

            img = rotated_image;
        }
    }

    FILE *out_file = fopen(out_img, "wb");

    if (!out_file) {
        perror("Can't open file for write");
        free(img.data);
        return FILE_WRITE_ERR;
    }

    enum write_status write_result = to_bmp(out_file, &img);
    fclose(out_file);
    free(img.data);

    if (write_result != WRITE_OK) {
        perror("Can't write image\n");
        return IMAGE_WRITE_ERR;
    } else {
        printf("Cool");
    }

    return EXITOK;

}

