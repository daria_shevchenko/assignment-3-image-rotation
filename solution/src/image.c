#include "image.h"
#include <stdio.h>
#include <stdlib.h>

static struct image make_rotate_image(uint64_t width, uint64_t height) {
    struct image rotateImg;
    rotateImg.width = height;
    rotateImg.height = width;
    rotateImg.data = (struct pixel*) malloc(rotateImg.width * rotateImg.height * sizeof(struct pixel));
    return rotateImg;
}

struct image rotate(struct image const source) {
    struct image rotateImg;
    rotateImg = make_rotate_image(source.width, source.height);
    for (uint64_t i = 0; i < source.width; i++) {
        for (uint64_t j = 0; j < source.height; j++) {
            if (source.data != NULL) {
                rotateImg.data[(rotateImg.height - i - 1) * rotateImg.width + j] = source.data[j * source.width + i];
            } else {
                perror("Problem");
            }
        }
    }
    return rotateImg;
}
