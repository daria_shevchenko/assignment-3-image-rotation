#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

// red green blue - pixel
struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

struct image rotate(struct image const source);

#endif  // IMAGE_H
