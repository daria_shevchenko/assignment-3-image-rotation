#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

#include "image.h"
#define BFTYPE 0x4D42
#define SIZE 40
#define BITCOUNT 24
#define BFRESERVED 0
#define BIPLANES 1
#define BICOMPRESSION 0
#define BISIZEIMAGE 0
#define BIXPELSPERMETER 0
#define BIYPELSPERMETER 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0
#define FILENUMBER 1
#define PADDINGNUMBER 4
#define IMGWIDTH 3
#define INPUT_FILE_NAME 1
#define OUTPUT_FILE_NAME 2
#define ANGLE 3
#define EXITOK 0
#define DEGREE 360
#define HALFDEGREE 90
#define CORRECTARGS 4
#define NEGANGLE 4
enum read_status {
    READ_OK = 0,
    READ_NUMBER,
    READ_INVALID_SIGNATURE
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};
enum write_status to_bmp(FILE *outFile, struct image const *img);
enum read_status from_bmp(FILE *inpFile, struct image *img);


#endif  // BMP_H
