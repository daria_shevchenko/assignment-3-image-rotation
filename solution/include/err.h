#ifndef ERR_H
#define ERR_H

#include <stdint.h>
#include <stdint.h>

enum err {
    BAD_ARGS = 0,
    IMAGE_READ_ERR,
    IMAGE_WRITE_ERR,
    FILE_ERR,
    FILE_WRITE_ERR,
    BAD_ANGLE
};

#endif
